variable "aws_region" {
  description = "AWS region"
  type        = string
  default     = "us-east-2"
}

variable "repository_name" {
    description = "Name of repository"
    type        = string
    default = "terminal"  
}