terraform {
  backend "s3" {
    bucket         = "aik-diploma-bucket"
    key            = "ecr/terraform.tfstate"
    region         = "us-east-2"
    dynamodb_table = "terraform-ecr-lock"
  }
}

provider "aws" {
  region = var.aws_region
}