resource "aws_dynamodb_table" "dynamodb-terraform-state-lock-ecs" {
  name           = "terraform-ecs-lock"
  hash_key       = "LockID"
  read_capacity  = 5
  write_capacity = 5

  attribute {
    name = "LockID"
    type = "S"
  }
}

resource "aws_dynamodb_table" "dynamodb-terraform-state-lock-ecr" {
  name           = "terraform-ecr-lock"
  hash_key       = "LockID"
  read_capacity  = 5
  write_capacity = 5

  attribute {
    name = "LockID"
    type = "S"
  }
}