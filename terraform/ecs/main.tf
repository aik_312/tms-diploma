terraform {
  backend "s3" {
    bucket         = "aik-diploma-bucket"
    key            = "ecs/terraform.tfstate"
    region         = "us-east-2"
    dynamodb_table = "terraform-ecs-lock"
  }
}

provider "aws" {
  region = var.aws_region
}

data "terraform_remote_state" "ecr_state_data" {
  backend = "s3"

  config = {
    bucket         = "aik-diploma-bucket"
    key            = "ecr/terraform.tfstate"
    region         = "us-east-2"
    dynamodb_table = "terraform-ecr-lock"
  }
}