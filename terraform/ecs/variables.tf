variable "aws_region" {
  description = "AWS region"
  type        = string
  default     = "us-east-2"
}

variable "server_port" {
  description   = "Server port"
  type          = string
  default       = 55555
}

variable "ecs_name" {
  description = "ECS Name"
  type        = string
  default     = "terminal-chat-server"
}

