data "template_file" "task_definition_template" {
    template = file("task_definition.json.tpl")
    vars = {
      REPOSITORY_URL = data.terraform_remote_state.ecr_state_data.outputs.url
      PORT = var.server_port
      ECS_NAME = var.ecs_name
      AWS_REGION = var.aws_region
    }
}

data "aws_vpc" "default" {
  filter {
    name = "tag:Name"
    values = ["default"]
  }
}

data "aws_subnet" "ecs-subnet" {
  filter {
    name = "tag:Name"
    values = ["terminal-chat-server"]
  }
}

data "aws_iam_instance_profile" "ecs_agent" {
  name = "ecsInstanceRole"
}

resource "aws_cloudwatch_log_group" "EcsWatchLogGroup" {
  name = "${var.ecs_name}LogGroup"
  retention_in_days = 1
  tags = {
    Name = "${var.ecs_name}LogGroup"
  }
}

resource "aws_ecs_cluster" "terminal-chat" {
  name = "terminal-chat-server"
}

resource "aws_ecs_task_definition" "terminal-chat" {
  family                = "terminal-chat-server"
  container_definitions = data.template_file.task_definition_template.rendered

  depends_on = [
    aws_cloudwatch_log_group.EcsWatchLogGroup
  ]
}

resource "aws_security_group" "ecs_sg" {
  name        = "terminal-chat-server"
  description = "Allow SSH and terminal-chat connections"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description      = "Allow connections to server terminal chat"
    from_port        = var.server_port
    to_port          = var.server_port
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "Allow SSH connections to cluster node"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = [data.aws_vpc.default.cidr_block] 
  }  

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = var.ecs_name
  }
}

resource "aws_launch_configuration" "ecs_launch_config" {
    image_id             = "ami-0102692edc680b5b0"
    iam_instance_profile = data.aws_iam_instance_profile.ecs_agent.name
    security_groups      = [aws_security_group.ecs_sg.id]
    user_data            = "#!/bin/bash\necho ECS_CLUSTER=terminal-chat-server >> /etc/ecs/ecs.config"
    instance_type        = "t2.micro"
    key_name             = "ecs-keys"

    depends_on = [
      aws_security_group.ecs_sg
    ]
}

resource "aws_autoscaling_group" "ecs_asg" {
    name                      = "terminal-chat"   
    launch_configuration      = aws_launch_configuration.ecs_launch_config.name
    vpc_zone_identifier       = [data.aws_subnet.ecs-subnet.id]
    desired_capacity          = 1
    min_size                  = 1
    max_size                  = 1
    health_check_grace_period = 300
    health_check_type         = "EC2"
    tag {
      key = "Name"
      value = "ECS-Instance"
      propagate_at_launch = true
    }
}

resource "aws_ecs_service" "terminal-chat" {
  name            = "terminal-chat-server"
  cluster         = aws_ecs_cluster.terminal-chat.id
  task_definition = aws_ecs_task_definition.terminal-chat.arn
  desired_count   = 1
  launch_type     = "EC2"
}