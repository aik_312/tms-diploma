#!/bin/bash
aws ecr describe-images --repository-name terminal-chat-server --image-ids imageTag=$1 &> /dev/null
if [ $(echo $?) -eq 0 ]; then
	echo "EXIST"
else
	echo "NOT_EXIST"
fi