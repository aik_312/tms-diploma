def DOCKER_REPO = "undefined"
def SERVER_IP_ADDRESS = "undefined"
def CLIENT_IP_ADDRESS = "undefined"
def CLUSTER_NODE_ID = "undefined"
def IMAGE_EXISTS = "false"

pipeline {
    agent {
        label 'aws'
    }

    environment {
        SLACK_CHANNEL = 'aik-messages'
        PORT = '31279'
        IMAGE_NAME_SERVER = 'terminal-chat-server'
        IMAGE_NAME_CLIENT = 'terminal-chat-client'
        PASSWORD_CREDENTIALS_ID = 'dockerhub'        
        AWS_ACCESS_KEY = credentials ('AccessKeyID')
        AWS_SECRET_KEY = credentials ('SecretAccessKey')
        AWS_REGION = 'us-east-2'  
        MASTER_SSH_ID = 'MasterSshKey'    
    }

    stages {
        stage("Download source code") {
            steps {
                dir('terminal-chat') {
                    git branch: 'master',
                    url: 'git@github.com:AiK312/terminal-chat.git',
                    credentialsId: 'github'
                }
            }
        }

        stage("Prerequisites") {
            steps {
                sh """
                    sudo yum -y update
                    sudo yum -y install gcc-c++ curl unzip yum-utils
                    sudo amazon-linux-extras install ansible2 -y
                    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
                    unzip -o awscliv2.zip
                    sudo ./aws/install --update                    
                    sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
                    sudo yum -y install terraform                    
                    printenv
                    aws --version
                    ansible --version
                """

            }
        }

        stage("Configure AWS") {
            steps {
                sh """
                    aws configure set aws_access_key_id ${AWS_ACCESS_KEY}
                    aws configure set aws_secret_access_key ${AWS_SECRET_KEY}
                    aws configure set default.region ${AWS_REGION}
                """
            }
        }

        stage("Creating ECR by Terraform") {
            steps {
                dir('terraform/ecr') {
                    sh """
                        echo "Create/update ECR"
                        pwd
                        terraform init -input=false                       
                        terraform validate
                        terraform plan -var repository_name=${IMAGE_NAME_SERVER} \
                        -var aws_region=${AWS_REGION} \
                        -out terraform.tfplan
                        terraform apply -auto-approve terraform.tfplan
                        terraform output -raw url > ecr.file
                    """
                    script {
                        DOCKER_REPO = readFile('ecr.file').trim()
                    }
                }
            }
        }

        stage("Get Version") {
            steps {
                echo "Get version from package.json"
                script {
                    packageJson = readJSON(file: 'terminal-chat/package.json')
                    VERSION_SERVER = packageJson.versionServer
                    VERSION_CLIENT = packageJson.versionClient
                }
            }
        }

        stage("Compile") {
            steps {
                sh """
                    g++ --version
                    g++ terminal-chat/chatclient.cpp -lpthread -o chatclient
                    g++ terminal-chat/chatserver.cpp -lpthread -o chatserver
                """
            }
        }

        stage("Login into ECR") {
            steps {
                echo "Login into Elastic Container Registry on AWS"
                dir('terraform/ecr') {
                    sh """                                             
                        aws ecr get-login-password --region us-east-2 | sudo docker login --username AWS --password-stdin ${DOCKER_REPO}
                    """
                }
            }
        }

        stage("Check if server image exists") {
            steps {
                sh """
                    sudo chmod +x ./is_image_exists.sh
                    RESULT=\$(./is_image_exists.sh ${VERSION_SERVER})
                    if [ \$RESULT = EXIST ]; then
                        echo true > exist.file
                    else
                        echo false > exist.file
                    fi                       
                """                                      
                script {
                    IMAGE_EXISTS = readFile('exist.file').trim()
                    echo IMAGE_EXISTS
                }
            }
        }

        stage("Build docker image") {
            when {
                expression { 
                    IMAGE_EXISTS == "false"
                }
            }
            steps {
                echo "Build server and client docker images"
                sh """
                    pwd                    
                    echo "Building image: ${IMAGE_NAME_SERVER}"
                    sudo docker build --build-arg PORT=${PORT} --label ${IMAGE_NAME_SERVER} --tag ${DOCKER_REPO}:${VERSION_SERVER} --tag ${DOCKER_REPO}:latest -f ./docker/server/Dockerfile .               
                """
            }
        }

        stage("Push docker image") {
            when {
                expression { 
                    IMAGE_EXISTS == "false"
                }
            }
            steps {
                withCredentials([usernamePassword(credentialsId: env.PASSWORD_CREDENTIALS_ID, usernameVariable: 'REPOSITORY_USERNAME', passwordVariable: 'REPOSITORY_PASSWORD')]) {
                    echo "Pushing images to docker hub registry"
                    sh """
                        sudo chmod +x ./is_image_exists.sh
                        RESULT=\$(./is_image_exists.sh ${VERSION_SERVER})
                        if [ \$RESULT = EXIST ]; then
                            echo "Image already exists in repository"
                        else
                            echo "Image does not exist"
                            sudo docker push "${DOCKER_REPO}:${VERSION_SERVER}"
                            sudo docker push "${DOCKER_REPO}:latest"
                        fi                       
                    """
                }
            }
        }

        stage("Creating ECS by Terraform") {
            steps {
                dir('terraform/ecs') {
                    sh """
                        echo "Create/update ECS"
                        terraform init -input=false                        
                        terraform validate
                        terraform plan -var aws_region=${AWS_REGION} \
                        -var server_port=${PORT} \
                        -var ecs_name=${IMAGE_NAME_SERVER} -out terraform.tfplan
                        terraform apply -auto-approve terraform.tfplan
                    """
                }
            }
        }

        stage("Copying file into client by Ansible") {
            steps {
                withCredentials([sshUserPrivateKey(credentialsId: env.MASTER_SSH_ID, usernameVariable : 'MASTER_SSH_USER', keyFileVariable: 'MASTER_SSH_KEY')]) {
                    dir('ansible') {  
                        sh "aws ec2 describe-instances --filters Name=tag:Name,Values=Jenkins-Master \
                        Name=instance-state-name,Values=running  --query Reservations[*].Instances[*].[PublicIpAddress] --output text > Client_IP.file"                      
                        script {
                            CLIENT_IP_ADDRESS = readFile('Client_IP.file').trim()
                            echo CLIENT_IP_ADDRESS
                        }
                        sh """
                            bash -c 'echo "Jenkins_Master ansible_host=${CLIENT_IP_ADDRESS}" >> hosts.txt'
                            cat hosts.txt
                            echo "Push terminal chat client"                        
                            ansible-playbook --private-key ${MASTER_SSH_KEY} \
                            -u ${MASTER_SSH_USER} \
                            -i hosts.txt \
                            --extra-vars "version=${VERSION_CLIENT}" \
                            playbook_copy.yml
                        """
                    }
                }
            }
        }

        stage("Get cluster node IP") {
            steps {
                sh """
                    #!/bin/sh
                    aws ec2 describe-instances --filters Name=tag:Name,Values=ECS-Instance \
                    Name=instance-state-name,Values=running  --query Reservations[*].Instances[*].[PublicIpAddress] --output text > Server_IP.file
                """
                script {
                    SERVER_IP_ADDRESS = readFile('Server_IP.file').trim()
                    echo SERVER_IP_ADDRESS
                }
            }
        }
    }

    post {
        failure {
            slackSend channel: "${ SLACK_CHANNEL }", color: 'danger', message: "${ env.JOB_NAME } ${ env.BUILD_NUMBER } failed (<${ env.BUILD_URL }|Open>)"
        }

        success {
            slackSend channel: "${ SLACK_CHANNEL }", color: 'good', message: "${ env.JOB_NAME} ${ env.BUILD_NUMBER } complete. IP and Port = ${SERVER_IP_ADDRESS} ${PORT} (<${ env.BUILD_URL }|Open>)"
        }
    }
}
